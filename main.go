package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

type Config struct {
	OrderBy []string `json:"order_by"`
}

type CombineSet struct {
	Name       string  `json:"name"`
	MoneySpent float32 `json:"moneySpent"`
	Category   string  `json:"category"`
}

type CategorySet struct {
	Category   string  `json:"category"`
	MoneySpent float32 `json:"moneySpent"`
}

type NameSet struct {
	Name       string  `json:"name"`
	MoneySpent float32 `json:"moneySpent"`
}

func GenericErrHandler(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func GetDir() string {
	dir, err := os.Getwd()
	GenericErrHandler(err)

	return dir + "/"
}

func LoadConfig(filename string) (Config, error) {

	bytes, err := ioutil.ReadFile(GetDir() + filename)
	if err != nil {
		return Config{}, err
	}

	var c Config
	err = json.Unmarshal(bytes, &c)
	if err != nil {
		return Config{}, err
	}

	return c, nil
}

func ReadFilesJson() []CombineSet {

	var path = GetDir() + "input-folder"
	files, err := ioutil.ReadDir(path)
	GenericErrHandler(err)

	var combineSet []CombineSet
	for _, f := range files {
		jsonFile, err := os.Open(path + "/" + f.Name())
		GenericErrHandler(err)
		defer jsonFile.Close()

		var combine CombineSet
		byteValue, _ := ioutil.ReadAll(jsonFile)
		json.Unmarshal(byteValue, &combine)

		combineSet = append(combineSet, combine)

	}
	return combineSet
}

func SortCombine() []CombineSet {

	combine := ReadFilesJson()
	m := make(map[string]float32)
	for _, u := range combine {
		m[u.Name+":"+u.Category] += u.MoneySpent
	}

	var combineSet []CombineSet

	for key, element := range m {
		var combine CombineSet
		var split = strings.Split(key, ":")

		combine.Name, combine.Category, combine.MoneySpent = split[0], split[1], element
		combineSet = append(combineSet, combine)
	}
	return combineSet
}

func SortByName() []NameSet {

	combineSet := ReadFilesJson()
	m := make(map[string]float32)

	for _, c := range combineSet {
		m[c.Name] += c.MoneySpent
	}

	var nSet []NameSet
	var ns NameSet

	for k, v := range m {
		ns.Name, ns.MoneySpent = k, v
		nSet = append(nSet, ns)
	}

	return nSet

}
func SortByCategory() []CategorySet {

	combineSet := ReadFilesJson()
	m := make(map[string]float32)

	for _, c := range combineSet {
		m[c.Category] += c.MoneySpent
	}

	var cSet []CategorySet
	var cs CategorySet

	for k, v := range m {
		cs.Category, cs.MoneySpent = k, v
		cSet = append(cSet, cs)
	}

	return cSet
}

func App() {

	c, err := LoadConfig("config.json")
	GenericErrHandler(err)

	if len(c.OrderBy) > 0 && len(c.OrderBy) < 3 {
		var isValid bool
		for _, v := range c.OrderBy {
			if v == "name" || v == "category" {
				isValid = true
			} else {
				fmt.Printf("%s is not a valid argument\n", v)
				isValid = false
				break
			}
		}
		if isValid {
			if len(c.OrderBy) == 1 {
				if c.OrderBy[0] == "category" {
					fmt.Println(SortByCategory())
				} else if c.OrderBy[0] == "name" {
					fmt.Println(SortByName())
				} else {
					log.Fatal("Suported config for 1 argument is name or category")
				}
			} else if len(c.OrderBy) == 2 {
				fmt.Println(SortCombine())
			}
		}
	} else {
		fmt.Printf("Found %d arguments in config file allowd number of args is 1-2\n", len(c.OrderBy))
	}
}

func main() {
	App()
}
